provider "aws" {
  # profile = "default"
  # shared_credentials_file = "~/.aws/credentials"
  # region                  = "us-east-1"
  access_key = "AKIAZ7XKWN5K3ILYAPUB"
  secret_key = "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"
  region     = "us-east-1"
}



variable "userName" {
  type = string
  default = "Swapnil-Terraform"
}

variable "Password" {
  type = string
  default = "Swapnil-PWD"
}




resource "aws_cloudformation_stack" "IAM_User" {
  name         = "User-Creation-Terraform"
  capabilities = ["CAPABILITY_NAMED_IAM"]
  on_failure   = "ROLLBACK"
  parameters = {
    UserName = var.userName
    Password = var.Password
  }
  template_body = <<STACK
  {
      "AWSTemplateFormatVersion": "2010-09-09",
      "Description": "IAM user",
      "Parameters": {
          "UserName": {
              "Type": "String"
          },
          "Password": {
              "Type": "String",
              "NoEcho": "true"
          }
      },
      "Resources": {
          "IAMUSER": {
              "Type": "AWS::IAM::User",
              "Properties": {
                  "LoginProfile": {
                      "Password": {
                          "Ref": "Password"
                      }
                  },
                  "UserName": {
                      "Ref": "UserName"
                  },
                  "ManagedPolicyArns": []
              }
          }
      },
      "Outputs": {
          "UserName": {
              "Description": "Newly created IAM username",
              "Value": {
                  "Ref": "UserName"
              }
          },
          "Password": {
              "Description": "Newly created IAM user password",
              "Value": {
                  "Ref": "Password"
              }
          }
      }
  }
    STACK

}
